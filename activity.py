# Class
class Camper():

    # Attributes 
	def __init__(self, name, batch, course_type):	
		self.name = name
		self.batch = batch
		self.course_type = course_type   

	# Method
	def career_track(self):
		print(f"Currently enrolled in the {self.course_type} program.")

	def info(self):
		print(f"My name is {self.name} of batch {self.batch}.")


# Object
zuitt_camper = Camper("Deniese", "276", "Intro to Python")


print(f"Camper Name: {zuitt_camper.name}")
print(f"Camper Batch: {zuitt_camper.batch}")
print(f"Camper Course: {zuitt_camper.course_type}")

# Execute the info method and career_track of the object.
zuitt_camper.info()
print(zuitt_camper.info)

zuitt_camper.career_track()
print(zuitt_camper.career_track)

